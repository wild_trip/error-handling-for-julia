/*������������ ���� 108 ������
5 ������� 20 �������
��������� ������ ����������� ��������� �� ������� ��� ������ ��� ��������� ����
�������, ����������� � ������� ����������� ������� �������� R*/


#include <iostream>
#include <vector>
#include <windows.h>
#include "resource.h"
#include <math.h>
using namespace std;

const double pi = 3.1415926;
const double zoom = 10; //zoom coefficient

struct Parametrs { // system parametrs
	double R;	// circle radius
	int CS_X;	// x �oordinate system offset
	int CS_Y;	// y �oordinate system offset
	double g;	// gravity constant
	double T;	// Duration
	int nT;		//Number of time intervals
} parametrs;

struct MaterialPoint { // phase space
	double x;
	double y;
	double vx;
	double vy;
	void velocityReflection(double angle) { //Turn of velocity clockwise
		//Turn of velocity clockwise (with turn matrix)
		double vxNext = vx * cos(angle) + vy * sin(angle);
		double vyNext = - vx * sin(angle) + vy * cos(angle);
		//Reflection
		vyNext = -vyNext;
		//Turn of velocity counterclock - wise (with turn inverse matrix)
		vx = vxNext * cos(angle) - vyNext * sin(angle);
		vy = vxNext * sin(angle) + vyNext * cos(angle);
	}
} startPoint;


void init() {
	parametrs.CS_X = 200;
	parametrs.CS_Y = 200;
	parametrs.g = - 9.8;
	parametrs.nT = 20000;
}

MaterialPoint getNextPoint(MaterialPoint &prevPoint, double dt) {
	MaterialPoint nextPoint;
	nextPoint.x = prevPoint.x + prevPoint.vx * dt;
	nextPoint.y = prevPoint.y + prevPoint.vy * dt;
	nextPoint.vx = prevPoint.vx;
	nextPoint.vy = prevPoint.vy + parametrs.g * dt;
	return nextPoint;
}

vector<MaterialPoint> systemSimulation(MaterialPoint startPoint, Parametrs sysParam) {
	double dt = sysParam.T / sysParam.nT;

	vector<MaterialPoint> way;
	cout << startPoint.x << " " << startPoint.y << endl;
	way.push_back(startPoint); // Start conditions

	for (int i = 0; i <= sysParam.nT; i++) {
		MaterialPoint nextPoint = getNextPoint(way.back(), dt);
		//���� ���������
		double distanceFromCenter = sqrt(nextPoint.x * nextPoint.x + nextPoint.y * nextPoint.y);
		if (distanceFromCenter >= parametrs.R) {
			double alpha = way.back().x / sqrt(way.back().x * way.back().x + way.back().y * way.back().y);
			way.back().velocityReflection(alpha);
			nextPoint = getNextPoint(way.back(), dt);
		}
		way.push_back(nextPoint);
	}

	return way;
}

int WINAPI DlgProc(HWND hDlg, WORD wMsg, WORD wParam, DWORD)
{
	PAINTSTRUCT ps;

	if (wMsg == WM_CLOSE || wMsg == WM_COMMAND && wParam == IDOK) {
		EndDialog(hDlg, 0);
	}
	else
		if (wMsg == WM_INITDIALOG) {

			RECT rc;
			GetClientRect(hDlg, &rc);
			int dx = rc.right - rc.left;
			int dy = rc.bottom - rc.top;
		}

		else
			if (wMsg == WM_PAINT) {
				BeginPaint(hDlg, &ps);

				HPEN hPen = (HPEN)CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
				HPEN hOldPen = (HPEN)SelectObject(ps.hdc, hPen);

				POINT ptOld;

				Ellipse(
					ps.hdc, 
					parametrs.CS_X - round(zoom * parametrs.R), parametrs.CS_Y - round(zoom*parametrs.R),
					parametrs.CS_X + round(zoom * parametrs.R), parametrs.CS_Y + round(zoom*parametrs.R)
				); //������� ������� 

				vector<MaterialPoint> simulationData = systemSimulation(startPoint, parametrs);
				//����� ������� ������������ ���� ������
				MoveToEx(
					ps.hdc, 
					parametrs.CS_X + round(zoom * startPoint.x), 
					parametrs.CS_Y - round(zoom * startPoint.y),
					&ptOld
				);

				for (auto point : simulationData) {
					//���������� ����������
					int x = parametrs.CS_X + round(zoom * point.x);
					int y = parametrs.CS_Y - round(zoom * point.y);
					LineTo(ps.hdc, x, y);
					cout 
						<< "Draw point: " << point.x << " " << point.y << " with velosity:" << point.vx << " " << point.vy <<endl;
				}

				SelectObject(ps.hdc, hOldPen);
				DeleteObject(hPen);
				EndPaint(hDlg, &ps);
			}
	return 0;
}

int main()
{

	string command;
	bool menu = true;
	setlocale(LC_ALL, "rus");

	init(); //Parameters initialization

	while (menu == true) {
		cout << "�� ������ ������� ��������? ���� ��, �� ������� yes, ���� ���, �� ������� no " << endl;
		cin >> command;
		if (command == "yes") {
			cout << "������� ��������� ����������" << endl;
			cin >> startPoint.x >> startPoint.y;
			cout << "������� ��������� ��������" << endl;
			cin >> startPoint.vx >> startPoint.vy;
			cout << "������� ������ �������" << endl;
			cin >> parametrs.R;
			cout << "������� ����� � " << endl;
			cin >> parametrs.T;
			break;
		}
		else if (command == "no") {
			startPoint.x = -5;
			startPoint.y = 7;
			startPoint.vx = 1;
			startPoint.vy = 2;
			parametrs.R = 20;
			parametrs.T = 50;
			break;
		}
	}

	DialogBox(NULL, MAKEINTRESOURCE(IDD_DIALOG1), NULL, (DLGPROC)DlgProc);

}

